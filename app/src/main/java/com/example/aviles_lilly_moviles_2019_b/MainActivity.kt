package com.example.aviles_lilly_moviles_2019_b

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.PersistableBundle
import android.util.Log

import android.widget.Button
import android.widget.TextView



class MainActivity : AppCompatActivity() {

    private lateinit var startButton: Button

    private lateinit var gameScoreTExtView: TextView
    private lateinit var timeLeftTextView: TextView
    private lateinit var  tapMeButton: Button

    private lateinit var countDownTimer: CountDownTimer

    private var countDownInterval:Long = 1000

    private var timeLeft = 10
    private var gameScore = 0
    private var isGameStarted = false

    private val TAG = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        Log.d(TAG,"onCreate called,score is $gameScore")


        gameScoreTExtView= findViewById(R.id.score_text_view)

        tapMeButton= findViewById(R.id.tapm)



        tapMeButton.setOnClickListener {

            incrementScore()}

        if (savedInstanceState != null) {
            gameScore = savedInstanceState.getInt(SCORE_KEY)

            isGameStarted = savedInstanceState.getBoolean(GAME_STARTED)
            restoreGame()
        } else {
            resetGame()
        }


    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

        setContentView(R.layout.activity_main)
        startButton = findViewById(R.id.start)
    }


    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)




        countDownTimer.cancel()

        Log.d(TAG, "onSaveInstanceState: Saving score: $gameScore and timeLeft: $timeLeft")

    }

    override fun onDestroy() {
        super.onDestroy()

        countDownTimer.cancel()
        Log.d(TAG, "onDestroy called")
    }


    private fun incrementScore(){
        if (!isGameStarted){
            startGame()
        }
        gameScore ++
        gameScoreTExtView.text=getString(R.string.score, gameScore)
    }

    private fun resetGame(){
        gameScore=0
        gameScoreTExtView.text=getString(R.string.score,gameScore)
        timeLeft=10


        configCountDownTimer()
        isGameStarted=false
    }
    private fun startGame() {
        countDownTimer.start()
        isGameStarted=true
    }


    private fun restoreGame () {
        gameScoreTExtView.text=getString(R.string.score, gameScore)
        configCountDownTimer()



        if (isGameStarted){
            countDownTimer.start()
        }

    }

    private fun configCountDownTimer (){
        countDownTimer= object  : CountDownTimer((timeLeft*1000).toLong(), countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeft=millisUntilFinished.toInt() / 1000

            }

        }
    }

    companion object {
        private const val SCORE_KEY = "SCORE_KEY"
        private const val GAME_STARTED= "GAME_STARTED"


    }
}
